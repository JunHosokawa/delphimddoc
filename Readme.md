﻿# Delphi Markdown Document Generator

Generate Markdown style documentation from Delphi source code.  
Delphiのソースコードから Markdown 形式のドキュメントを生成します。  

## Usage
**1.Specify the file name**

```bat
DelphiMdDoc D:\Sample\Foo.pas
```

Generate ```Foo.md``` .

**2.Specify the folder name**

```bat
DelphiMdDoc D:\Sample
```
If ```Foo.dpr```, ```Bar.pas```, ```Baz.inc``` exists in the specified folder, ```Foo.md```, ```Bar.md```, ```Baz.md``` is generated.

### Generate Sample

The image is reduced and it is difficult to see it. Please see it in its full size (right-click the image "Open image in a new tab", etc.)  
画像が潰れてしまっているので、原寸大でご覧ください（右クリック「新しいタブで画像を開く」など）  

![Sample](sample.png)

# License
Copyright (C) 2019 pik. (Twitter: @pik)  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
